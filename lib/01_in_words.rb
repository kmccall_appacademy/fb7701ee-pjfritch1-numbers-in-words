class Fixnum
  def in_words

    if self == 0
      return "zero"
    end
    
    string = ''

    ones = ['one','two','three','four','five','six','seven','eight','nine']
    tens = ['ten','twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety']
    teens = ['eleven','twelve','thirteen','fourteen', 'fifteen','sixteen','seventeen','eighteen','nineteen']


    digits_remaining = self
    write = digits_remaining/1000000000000
    digits_remaining = digits_remaining - write*1000000000000

    if write > 0
      trillions = write.in_words
      string = string + trillions + " trillion "
    end

    write = digits_remaining/1000000000
    digits_remaining = digits_remaining - write*1000000000

    if write > 0
      billions = write.in_words
      string = string + billions + " billion "
    end

    write = digits_remaining/1000000
    digits_remaining = digits_remaining - write*1000000

    if write > 0
      millions = write.in_words
      string = string + millions + " million "
    end

    write = digits_remaining/1000
    digits_remaining = digits_remaining - write*1000

    if write > 0
      thousands = write.in_words
      string = string + thousands + ' thousand '
    end

    write = digits_remaining/100
    digits_remaining  = digits_remaining - write*100 #

    if write > 0
      hundreds  = write.in_words
      string = string + hundreds + ' hundred '
    end

    write = digits_remaining/10
    digits_remaining  = digits_remaining - write*10

    if write > 0
      if ((write == 1) and (digits_remaining > 0))
        string = string + teens[digits_remaining-1]
        digits_remaining = 0
      else
        string = string + tens[write-1]
      end

      if digits_remaining > 0
        string = string + ' '
      end
    end

    write = digits_remaining
    digits_remaining  = 0

    if write > 0
      string = string + ones[write-1]
    end

    string.strip
  end
end
